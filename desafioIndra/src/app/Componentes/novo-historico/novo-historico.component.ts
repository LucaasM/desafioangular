import { Component, OnInit } from '@angular/core';

import {CombustivelService} from '../../services/combustivel.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';

@Component({
  selector: 'app-novo-historico',
  templateUrl: './novo-historico.component.html',
  styleUrls: ['./novo-historico.component.css']
})
export class NovoHistoricoComponent implements OnInit {

  id;
  form: FormGroup;
 

  constructor(private combustivelService : CombustivelService, private formBuilder: FormBuilder, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() 
  {
    this.form = this.formBuilder.group({
      combustivel: [null, Validators.required],
      data: [null, Validators.required],
      preco: [null, Validators.required]
    }

    );

    this.route.params.subscribe(
      (params: any) => {
        const id = params['id'];
        console.log(id);
        const dados = this.combustivelService.loadByID(id);
        dados.subscribe(dado => {
          this.updateForm(dado);

        })
      }
    )
  }
  
  updateForm(dado){
    this.form.patchValue( {
      combustivel: dado.combustivel,
      data: dado.data,
      preco: dado.preco
    });
  }

  

  onSubmit(){
    if(this.form.valid){
  
        this.combustivelService.addHistorico(this.form.value).subscribe(res =>
          res => console.log('sucesso'),
          error => console.log(error)
        );
        
      }
     
    }
  


}
  

