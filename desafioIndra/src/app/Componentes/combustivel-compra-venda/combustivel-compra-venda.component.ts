import { CombustivelCompraVendaModule } from './../../models/combustivel-compra-venda/combustivel-compra-venda.module';
import { Component, OnInit } from '@angular/core';


import {CombustivelService} from '../../services/combustivel.service';

@Component({
  selector: 'app-combustivel-compra-venda',
  templateUrl: './combustivel-compra-venda.component.html',
  styleUrls: ['./combustivel-compra-venda.component.css']
})
export class CombustivelCompraVendaComponent implements OnInit {
  
  precoMedio: CombustivelCompraVendaModule;
  constructor(private combustivelService : CombustivelService) { }

  ngOnInit(){
    this.getCombustivelCompraVenda();
    
  }

  getCombustivelCompraVenda () {
    return this.combustivelService.getCombustivelCompraVenda().subscribe(dados => {
      this.precoMedio = dados;
    })

}

}
