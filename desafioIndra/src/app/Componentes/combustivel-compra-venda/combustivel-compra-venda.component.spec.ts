import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CombustivelCompraVendaComponent } from './combustivel-compra-venda.component';

describe('CombustivelCompraVendaComponent', () => {
  let component: CombustivelCompraVendaComponent;
  let fixture: ComponentFixture<CombustivelCompraVendaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CombustivelCompraVendaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CombustivelCompraVendaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
