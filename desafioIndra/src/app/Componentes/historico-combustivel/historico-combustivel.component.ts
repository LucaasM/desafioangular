import { FormGroup, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { HistoricoCombustivelModule } from '../../models/historico-combustivel/historico-combustivel.module';
import { Component, OnInit } from '@angular/core';

import {CombustivelService} from '../../services/combustivel.service'
 
@Component({
  selector: 'app-historico-combustivel',
  templateUrl: './historico-combustivel.component.html',
  styleUrls: ['./historico-combustivel.component.css']
})
export class HistoricoCombustivelComponent implements OnInit {

  form: FormGroup;
  historicoCombustivel: HistoricoCombustivelModule[];
  constructor(private combustivelService: CombustivelService, private router: Router, private route: ActivatedRoute, private formBuilder: FormBuilder) { }

  ngOnInit() 
  {
    this.getHistoricoCombustivel()


  }

  getHistoricoCombustivel (){
    return this.combustivelService.getHistoricoCombustivel().subscribe(dados =>{
      this.historicoCombustivel = dados;
    })
  }

   onEdit(id) {
    
    this.router.navigate(['historico', id]), { relativeTo: this.route};      
  }
}
