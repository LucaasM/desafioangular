import { PrecoMunicipioModule } from './../../models/preco-municipio/preco-municipio.module';
import { Component, OnInit } from '@angular/core';

import {CombustivelService} from '../../services/combustivel.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-preco-municipio',
  templateUrl: './preco-municipio.component.html',
  styleUrls: ['./preco-municipio.component.css']
})
export class PrecoMunicipioComponent implements OnInit {

  municipio: string;
  muni: string;
  precoMuni: number;
  
  constructor(private combustivelService : CombustivelService, private route: ActivatedRoute) {
    
   }

  ngOnInit(){
    

    this.route.params.subscribe(
      (params: any) => {
        this.municipio = params['municipio']
      }
    )

    console.log(this.getPrecoMunicipio(this.municipio));
  }

  getPrecoMunicipio(minucipio){
    this.muni = this.municipio.toUpperCase();
    console.log(this.muni)
    return this.combustivelService.getPrecoMunicipio(this.muni).subscribe(dados => {
      this.precoMuni = dados;
    })
  }

}
