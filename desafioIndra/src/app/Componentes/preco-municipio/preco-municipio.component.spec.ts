import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrecoMunicipioComponent } from './preco-municipio.component';

describe('PrecoMunicipioComponent', () => {
  let component: PrecoMunicipioComponent;
  let fixture: ComponentFixture<PrecoMunicipioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrecoMunicipioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrecoMunicipioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
