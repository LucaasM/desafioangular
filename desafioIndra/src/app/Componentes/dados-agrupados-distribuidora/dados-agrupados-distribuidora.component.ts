import { DadosAgrupadosDistribuidoraModule } from './../../models/dados-agrupados-distribuidora/dados-agrupados-distribuidora.module';
import { Component, OnInit } from '@angular/core';

import {CombustivelService} from '../../services/combustivel.service';

@Component({
  selector: 'app-dados-agrupados-distribuidora',
  templateUrl: './dados-agrupados-distribuidora.component.html',
  styleUrls: ['./dados-agrupados-distribuidora.component.css']
})
export class DadosAgrupadosDistribuidoraComponent implements OnInit {

  dadosAgrupados: DadosAgrupadosDistribuidoraModule[];
  public paginaAtual = 1;

  constructor(private combustivelService : CombustivelService) { }

  ngOnInit(){
    this.getDadosAgrupadosDistribuidora();
  }

  getDadosAgrupadosDistribuidora(){
    return this.combustivelService.getDadosAgrupadosDistribuidora().subscribe(dados => {
      this.dadosAgrupados = dados;
    })
  }

}
