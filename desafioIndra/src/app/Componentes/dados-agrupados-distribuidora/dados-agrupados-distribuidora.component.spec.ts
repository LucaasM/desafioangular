import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DadosAgrupadosDistribuidoraComponent } from './dados-agrupados-distribuidora.component';

describe('DadosAgrupadosDistribuidoraComponent', () => {
  let component: DadosAgrupadosDistribuidoraComponent;
  let fixture: ComponentFixture<DadosAgrupadosDistribuidoraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DadosAgrupadosDistribuidoraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DadosAgrupadosDistribuidoraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
