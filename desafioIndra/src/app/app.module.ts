import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgxPaginationModule } from 'ngx-pagination';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';


import {HttpClientModule} from '@angular/common/http';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';


import {CombustivelService} from './services/combustivel.service';
import { CombustivelCompraVendaComponent } from './Componentes/combustivel-compra-venda/combustivel-compra-venda.component';
import { DadosAgrupadosDistribuidoraComponent } from './Componentes/dados-agrupados-distribuidora/dados-agrupados-distribuidora.component';
import { HistoricoCombustivelComponent } from './Componentes/historico-combustivel/historico-combustivel.component';
import { PrecoMunicipioComponent } from './Componentes/preco-municipio/preco-municipio.component';
import { NovoHistoricoComponent } from './Componentes/novo-historico/novo-historico.component';

@NgModule({
  declarations: [
    AppComponent,
    CombustivelCompraVendaComponent,
    DadosAgrupadosDistribuidoraComponent,
    HistoricoCombustivelComponent,
    PrecoMunicipioComponent,
    NovoHistoricoComponent,

   
   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    NgxPaginationModule,
    ReactiveFormsModule

  ],
  providers: [CombustivelService],
  bootstrap: [AppComponent]
})
export class AppModule { }
