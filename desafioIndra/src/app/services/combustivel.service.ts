import { Injectable } from '@angular/core';

import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from 'rxjs';
import { CombustivelCompraVendaModule } from '../models/combustivel-compra-venda/combustivel-compra-venda.module';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class CombustivelService {

  url = "https://combustivelapp.herokuapp.com/api";
  municipio: string;
  historico: object;
  constructor(private httpClient : HttpClient) { }

  getCombustivelCompraVenda (): Observable<CombustivelCompraVendaModule>{
    return this.httpClient.get<CombustivelCompraVendaModule>(`${this.url}/combustivel/valor-media-compra-venda-municipio`);
  }

  getDadosAgrupadosDistribuidora (){
    return this.httpClient.get<any>(`${this.url}/combustivel/dados-agrupados-por-distribuidora`)
  }

  getHistoricoCombustivel (){
    return this.httpClient.get<any>(`${this.url}/historico`);
  }

  getPrecoMunicipio (municipio) 
  {
    this.municipio = municipio;
    return this.httpClient.get<any>(`${this.url}/combustivel/media-de-preco/${this.municipio}`)
  }


  addHistorico (historico) {
    this.historico = {
      "combustivel": historico.combustivel,
      "data": historico.data,
      "preco": historico.preco
    }

    console.log(this.historico)
    return this.httpClient.post(`${this.url}/historico`, this.historico).pipe();
  }


  loadByID(id){
    return this.httpClient.get(`${this.url}/historico/${id}`).pipe();
  }

  updateHistorico (historico) {
    this.historico = {
      "combustivel": historico.combustivel,
      "data": historico.data,
      "preco": historico.preco
    }
    console.log(historico.id)
    return this.httpClient.put(`${this.url}/historico/${historico.id}`, this.historico).pipe();
  }

  


  delHistorico (id) {
    this.httpClient.delete(`${this.url}/historico/${id}`);
  }
}
