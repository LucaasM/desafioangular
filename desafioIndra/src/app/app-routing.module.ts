import { NovoHistoricoComponent } from './Componentes/novo-historico/novo-historico.component';
import { PrecoMunicipioComponent } from './Componentes/preco-municipio/preco-municipio.component';
import { HistoricoCombustivelComponent } from './Componentes/historico-combustivel/historico-combustivel.component';


import { CombustivelCompraVendaComponent } from './Componentes/combustivel-compra-venda/combustivel-compra-venda.component';
import { DadosAgrupadosDistribuidoraComponent } from './Componentes/dados-agrupados-distribuidora/dados-agrupados-distribuidora.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {path: '',  component:  HistoricoCombustivelComponent}, 
  {path: 'dados-agrupados', component: DadosAgrupadosDistribuidoraComponent},
  {path: 'valor-compra-venda', component: CombustivelCompraVendaComponent},
  {path: 'media-de-preco/:municipio', component: PrecoMunicipioComponent},
  {path: 'novo-historico', component: NovoHistoricoComponent},
  {path: 'historico/:id', component: NovoHistoricoComponent}



];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
