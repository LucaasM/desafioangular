import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class CombustivelCompraVendaModule { 
  atributo: string;
  mediaValorCompra: number;
  mediaValorVenda: number;
}
