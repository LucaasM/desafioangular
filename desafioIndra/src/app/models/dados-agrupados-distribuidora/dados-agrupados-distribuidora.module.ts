import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class DadosAgrupadosDistribuidoraModule {
  bandeira: string;
  codInstalacao: number;
  dataColeta: string;
  id: number;
  muninicipio: string;
  produto: string;
  revendedora: string;
  siglaEstado: string;
  siglaRegiao: string;
  unidadeMedida: string;
  valorCompra: number;
  valorVenda: number;

 }
